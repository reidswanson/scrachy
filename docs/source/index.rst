.. scrachy documentation master file, created by
   sphinx-quickstart on Thu Sep 17 17:07:10 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

*******************************
Scrachy |version| Documentation
*******************************

`Scrachy <https://bitbucket.org/reidswanson/scrachy/src/master/>`__ is a flexible cache storage backend for `Scrapy <https://scrapy.org/>`__ that saves its data in a relational database using `SQLAlchemy <https://www.sqlalchemy.org/>`__.
It also comes with a few extra utilities described below.

.. note::
   This version of Scrachy is a major reworking that adds and removes significant functionality and is incompatible with earlier versions.

Install
=======

You can install the latest version from git:

.. code-block:: bash

   >pip install git+https://bitbucket.org/reidswanson/scrachy.git

or from PyPI

.. code-block:: bash

   >pip install scrachy

Cache Storage
=============

To (minimally) use the storage backend you simply need to enable caching by adding the following to your ``settings.py`` file:

.. code-block:: python

   # Enable caching
   HTTPCACHE_ENABLED = True

   # Set the storage backend to the one provided by Scrachy.
   HTTPCACHE_STORAGE = 'scrachy.middleware.httpcache.AlchemyCacheStorage'

   # One of the supported SqlAlchemy dialects
   SCRACHY_DB_DIALECT = <database-dialect>

   # The name of the driver (that must be installed as an extra) and used.
   SCRACHY_DB_DRIVER = <database-driver>

   # Options for connecting to the database
   SCRACHY_DB_HOST = <database-hostname>
   SCRACHY_DB_PORT = <database-port>
   SCRACHY_DB_SCHEMA = <database-schema>
   SCRACHY_DB_DATABASE = <database-name>
   SCRACHY_DB_USERNAME = <username>

   # Note, do not store this value in the settings file. Use an environment
   # variable or python-dotenv.
   SCRACHY_DB_PASSWORD = <password>

   # A dictionary of other connection arguments
   SCRACHY_DB_CONNECT_ARGS

   # there may be a conflict with the compression middleware. If you encounter
   # errors either disable it or move it after the caching middleware.
   DOWNLOADER_MIDDLEWARES = {
       ...
       'scrapy.downloadermiddlewares.http.compression.HttpCompressionMiddleware': None,
   }

Selenium
========

There are two Selenium middleware classes provided by Scrachy.
To use them, first add one of them to the ``DOWNLOADER_MIDDLEWARES``

.. code-block:: python

   DOWNLOADER_MIDDLEWARES = {
        ...
        'scrachy.middleware.selenium.SeleniumMiddleware': 800,  # or AsyncSeleniumMiddleware
        ...
   }

Then in your spider parsing code use a :class:`~scrachy.http_.SeleniumRequest` instead of a :class:`scrapy.http.Request`.

For more details see the :ref:`Selenium <selenium_features>` section under **Features** or the API reference :mod:`documentation <scrachy.middleware.selenium>`.


License
=======
Scrachy is released using the GNU Lesser General Public License.
See the `LICENSE <https://bitbucket.org/reidswanson/scrachy/src/master/LICENSE.md>`__ file for more details.

Files that are adapted or use code from other sources are indicated either at the top of the file or at the location of the code snippet.
Some of these files were adapted from code released under a 3-clause BSD license.
Those files should indicate the original copyright in a comment at the top of the file.
See the `BSD_LICENSE <https://bitbucket.org/reidswanson/scrachy/src/master/BSD_LICENSE.md>`__ file for details of this license.


.. toctree::
   :maxdepth: 4
   :caption: Contents

   features
   dependencies
   api

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
