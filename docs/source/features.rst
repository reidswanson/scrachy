********
Features
********

The two main features of scrachy are a :ref:`cache storage backend <cache_storage>` that writes to a relational database using `SqlAlchemy <https://www.sqlalchemy.org/>`__ and a middleware that allow downloading requests using :ref:`Selenium <selenium_features>`.
It also provides a number of other related features which are described below.

See the :mod:`API documentation <scrachy.settings.defaults>` for a complete list of available settings.

.. _cache_storage:

Cache Storage
=============

SqlAlchemy Backend
--------------------------
:class:`~scrachy.middleware.httpcache.AlchemyCacheStorage` is a cache storage that stores the responses in a database via `SqlAlchemy <https://www.sqlalchemy.org/>`__.
It behaves similar to :class:`~scrapy.extensions.httpcache.DbmCacheStorage`, with some notable differences.

    * It stores the data as text instead of bytes, which limits it applicability to subclasses of :class:`~scrapy.http.TextResponse`.
    * It's not clear why the Scrapy backends perform their own expiration management aside from the configured cache policy, but it does and so does Scrachy.
      However, Scrachy uses a more complicated process for determining if a response is stale, which is described in the :ref:`expiration management <expiration_management>` section.
    * You can optionally extract the textual content from the page described in the :ref:`content extraction <content_extraction>` section.
    * It is possible to save the full scrape history.

The middleware returns a subclass of :class:`~scrapy.http.TextResponse` with a :class:`~scrachy.http_.CachedResponseMixin`.
The mixin adds the following attributes (which may or may not be set depending on the :ref:`alchemy_settings`):

    scrape_timestamp
        The most recent time the page was scraped.
    extracted_text
        The text extracted from page using a :class:`~scrachy.content.ContentExtractor`.
    body_length
        The number of bytes in the response body
    extracted_text_length
        The number of bytes in the extracted text
    scrape_history
        A list of :class:`scrachy.db.models.ScrapeHistory` objects contain the response body each time the page was scraped.

Activate the middleware as follows:

.. code-block:: python

    HTTPCACHE_STORAGE = 'scrachy.middleware.httpcache.AlchemyCacheStorage`

.. _alchemy_settings:

Settings
^^^^^^^^

.. literalinclude:: ../../scrachy/settings/defaults/storage.py
    :lines: 73-176


.. _expiration_management:

Expiration Management
---------------------
In scrapy a cached response becomes stale if it has been in the cache longer than ``EXPIRATION_SECS`` seconds.
Scrachy adds additional functionality to control when items are considered stale.
The 3 primary ways an item is considered stale are:

   expiration
      A response has been in the cache for too long.
   activation
      A response has not been in the cache long enough.
   schedule
      A response expires at a specific time or date using `cron <https://en.wikipedia.org/wiki/Cron>`__ semantics.

For each of these 3 methods a response can be marked stale via a global setting or by a pattern matching its URL.
A request is considered fresh (i.e., not stale) if:

   1. It has been in the cache longer than its activation period.
   2. It has been in the cache less than its expiration period.
   3. The scrape time is less than the expiration date derived from the schedule.

Settings
^^^^^^^^

.. literalinclude:: ../../scrachy/settings/defaults/storage.py
    :lines: 32-33, 36-71

.. _content_extraction:

Content Extraction
------------------
Scrachy can try to extract the textual content from the page and store it along with the full body using a :class:`scrachy.content.ContentExtractor`.

Scrachy comes with two content extractors.

    :class:`scrachy.content.bs4.BeautifulSoupExtractor`
        This uses `Beautiful Soup <https://www.crummy.com/software/BeautifulSoup/>`__ to extract the text content using a few simple rules to exclude various nodes from the DOM.

    :class: `scrachy.content.boilerpipe.BoilerpipeExtractor`
        This extractor uses `BoilerPy3 <https://github.com/jmriebold/BoilerPy3>`__ to try to remove boilerplate elements, such as headers, footers and navigation, before extracting the text.

To activate this feature and save extracted text in the cache set the :const:`~scrachy.settings.defaults.storage.SCRACHY_CONTENT_EXTRACTOR` setting to a non ``None`` value.

Settings
^^^^^^^^

.. literalinclude:: ../../scrachy/settings/defaults/storage.py
    :lines: 30, 179-203

Cache Policy
------------
Scrachy provides an :class:`~scrachy.middleware.httpcache.BlacklistPolicy`.
It wraps around any other scrapy cache policy as long as it accepts a :class:`~scrapy.settings.Settings` object as its first (and only) constructor parameter.

URLs are never cached if they match any of the patterns specified by the :const:`~scrachy.settings.defaults.policy.SCRACHY_POLICY_EXCLUDE_URL_PATTERNS` setting.
According to this policy, an item should be cached if the base class says it should be cached and the url does not match any of the specified patterns.

Activate the policy as follows:

.. code-block:: python

   HTTPCACHE_POLICY = 'scrachy.middleware.httpcache.BlacklistPolicy'

Settings
^^^^^^^^

.. literalinclude:: ../../scrachy/settings/defaults/policy.py
    :lines: 28-42


Request Fingerprinting
----------------------

Scrachy includes a more efficient ``RequestFingerprinter``, based on the 2.7 implementation provided by Scrapy 2.11.0.
It uses the same algorithm and data to fingerprint a request, but uses `msgspec <https://jcristharif.com/msgspec/>`__ instead of ``json`` to serialize the data and allows you to customize which hash function is used.
The use of ``msgspec`` provides about a 30% improvement in performance while using a different hash algorithm, such as `xxhash <https://github.com/Cyan4973/xxHash>`__, can speed up the fingerprinting by over 50x.
However, unless you are scraping millions of pages this speedup will have little practical effect.

Activate the policy as follows:

.. code-block:: python

   REQUEST_FINGERPRINTER_CLASS = 'scrachy.utils.request.DynamicHashRequestFingerprinter'

Settings
^^^^^^^^

.. literalinclude:: ../../scrachy/settings/defaults/fingerprinter.py
    :lines: 26-30

Response Filtering
------------------
Some scraping jobs require periodically scraping a domain looking for new content.
In these cases it is a waste of resources to reparse the existing pages.
:class:`~scrachy.middleware.filter.CachedResponseFilter` is a downloader middleware that will cause requests with a fresh response in the cache to be ignored.
There are 3 ways that you can prevent a fresh cached response from being ignored.

You can choose not to ignore cached requests using 3 methods.

  1. Set the ``request.meta`` attribute ``dont_cache`` is ``True``.
  2. Set the ``request.meta`` attribute ``dont_filter`` is ``True``
  3. If its url matches a regular expression in the settings variable :const:`~scrachy.settings.defaults.filter.SCRACHY_CACHED_RESPONSE_FILTER_EXCLUSIONS`.

Activate this middleware as follows:

.. code-block:: python

   DOWNLOADER_MIDDLEWARES = {
       # You probably want this early in the pipeline, because there's no point
       # in the other middleware if it is in the cache and we are going to
       # ignore it anyway.
       'scrachy.middleware.filter.FilterCachedResponse': 50,
       ...
   }

Settings
^^^^^^^^

.. literalinclude:: ../../scrachy/settings/defaults/filter.py
    :lines: 26-29

.. _selenium_features:

Selenium
========

Scrachy also provides support for using `Selenium <https://pypi.org/project/selenium/>`__ to download requests.
It essentially forks `scrapy-selenium <https://github.com/clemfromspace/scrapy-selenium>`__ and adds a few enhancements.

The `Scrapy Selenium Guide <https://scrapeops.io/python-scrapy-playbook/scrapy-selenium/>`__ provides a good overview of the basic usage.
The primary difference between the implementations is how ``scripts`` are handled.
With ``Scrapy-Selenium`` you pass javascript directly as a string to the ``script`` parameter of of a ``SeleniumRequest``.
With ``Scrachy`` you pass a :class:``~scrachy.http_.ScriptExecutor``, which is any callable that accepts a ``WebDriver`` and a ``Request`` as parameters.
The ``ScriptExecutor`` can optionally return a ``Response``, a ``list[Response]`` or a ``dict[str, Response]``, which if present will be made available in the ``request.meta`` attribute using the key ``script_result``.

There are two available selenium middlewares.

   :class:`scrachy.middleware.selenium.SeleniumMiddleware`
        This class is essentially the same as ``Scrapy-Selenium`` with the changes described above.

   :class:`scrachy.middleware.selenium.AsyncSeleniumMiddleware`
        This class uses `twisted <https://docs.twisted.org/en/stable/core/howto/process.html>`__ to spawn multiple processes to create a pool of WebDrivers that can handle requests concurrently.
        This can significantly increase the throughput, but is potentially less robust.

In theory, any supported WebDriver should work, but ``Chrome`` and ``Firefox`` are the safest bet.
Activate this middleware as follows:

.. code-block:: python

   DOWNLOADER_MIDDLEWARES = {
        ...
        'scrachy.middleware.selenium.SeleniumMiddleware': 800,  # or AsyncSeleniumMiddleware
        ...
   }


Settings
--------

.. literalinclude:: ../../scrachy/settings/defaults/selenium.py
    :lines: 26-55
