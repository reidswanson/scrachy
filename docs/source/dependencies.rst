************
Dependencies
************

========
Required
========

The following packages are required for basic functionality.

    * `cron-converter <https://pypi.org/project/cron-converter/>`__
    * `msgspec <https://jcristharif.com/msgspec/>`__
    * `scrapy <https://docs.scrapy.org/en/latest/index.html>`__
    * `selenium <https://pypi.org/project/selenium/>`__
    * `sqlalchemy <https://www.sqlalchemy.org/>`__
    * `pywin32 <https://pypi.org/project/pywin32/>`__ [Windows only]

======
Extras
======

| The following extras can be installed to provide additional features or install them all using
| ``pip install scrachy[all]``.

    * **content_extraction**
        * `beautifulsoup4 <https://beautiful-soup-4.readthedocs.io/en/latest/>`__
        * `boilerpy3 <https://github.com/jmriebold/BoilerPy3>`__
    * **html_parsing**
        * `html5lib <https://github.com/html5lib/html5lib-python>`__
        * `lxml <https://pypi.org/project/lxml/>`__
    * **mysql**
        * `pymysql <https://pypi.org/project/pymysql/>`__
    * **postgresql**
        * `psycopg2 <https://pypi.org/project/psycopg2/>`__
    * **testing**
        * `pytest <https://docs.pytest.org/en/7.4.x/>`__
        * `pytest-twisted <https://pypi.org/project/pytest-twisted/>`__
        * `python-dotenv <https://pypi.org/project/python-dotenv/>`__
        * `pyyaml <https://pypi.org/project/PyYAML/>`__

Any database driver supported by `SqlAlchemy Dialects <https://docs.sqlalchemy.org/en/20/dialects/index.html>`__ should work, but are not provided as extras.

========
Optional
========

To get the best performance out of the :class:`~scrachy.utils.request.DynamicHashRequestFingerprinter` you should install a fast hashing algorithm.

    * `xxhash <https://pypi.org/project/xxhash/>`__
    * `spookyhash <https://pypi.org/project/spookyhash/>`__
